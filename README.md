# Gusto Coffee API ☕

## Prérequis 🔧

- Node 10.x.x
- NPM / Yarn
- Télephone Android ou émulateur (Windows)
- Téléphone IOS ou émulateur (Mac)

## Installation 🔄

```
git clone
```

```
cd <projet>
```

```
npm install
```

### Mode développement

<br>
Modifier `url_localhost` du fichier `info.json` à la racine du projet.

Modifier la variable `url_api` à `url_localhost` dans `utils/API.js` et `utils/SocketIO.js` (commentez l'option `{ secure : true }` si vous êtes avec l'url en local).

## Lancement 🚀

```
npm run android || npm run ios
```
