import React, { useEffect } from 'react';
import SyncStorage from 'sync-storage';
import { AuthProvider } from './src/hooks/useAuth';
import StackNavigator from './src/navigation/StackNavigator';

// * Pour générer l'APK : éxecuter la commande ./gradlew clean assembleRelease en etant dans le dossier Android

const App = () => {
  useEffect(() => {
    // initialisation du stockage local
    SyncStorage.init();
  }, []);

  return (
    <AuthProvider>
      <StackNavigator />
    </AuthProvider>
  );
};

export default App;
