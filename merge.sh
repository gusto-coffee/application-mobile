#!/usr/bin/env bash

target_branch=$1

if [[ -z "$target_branch" ]]
then
  echo "Please specify a target"
  echo "usage : sh $0 <target-branch>"
  exit
fi


source_branch=$(git symbolic-ref --short -q HEAD)

# Ensure working directory in version branch clean
git update-index -q --refresh
if ! git diff-index --quiet HEAD --; then
  echo "Working directory not clean, please commit your changes first or stash them"
  exit
fi

git checkout ${target_branch}
git merge ${source_branch} --no-ff
git push

git checkout ${source_branch}
# Si on a des changements sur la target branch
if  ! git diff-index --quiet HEAD --;
then
# On merge source branch into target branch
  git merge ${target_branch} --no-ff
  git push
# Sinon on affiche qu'il n'y a pas besoin de merge
else
  echo "No changes detected so don't need to merge ${target_branch} into ${source_branch}"
  exit
fi
