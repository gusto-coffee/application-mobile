import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, FlatList } from 'react-native';
import { getProductsCategory } from '../../utils/API';
import CardProduct from '../../components/Cards/CardProduct';
import { HandleError } from '../../utils/HandleMessage';
import { Container } from '../../components/Styles/Theme';
import EmptyItem from '../../components/EmptyItem';

/**
 * Page qui affiche tous les produits correspondant à une catégorie
 */
const CategoryProduct = ({ route, navigation }) => {
  const [state, setState] = useState([]);
  const [dataLoaded, setDataLoaded] = useState(false);

  useEffect(() => {
    getProductsCategory(route.params.category._id)
      .then(({ status, data }) => {
        if (status === 200) {
          setState(data);
          setDataLoaded(true);
        }
      })
      .catch(e => HandleError('Error lors de la récupération des produits', e.response.data.error, e.response.status));
  }, [route.params.category._id]);

  if (!dataLoaded) return <View />;
  if (state.length === 0) return <EmptyItem message='Aucun produit' logoName='mug-hot' />;
  return (
    <Container alignItems='center'>
      <FlatList
        ListHeaderComponent={<Text style={styles.header}>{route.params.category.name}</Text>}
        contentContainerStyle={{ paddingBottom: '5%' }}
        columnWrapperStyle={{ justifyContent: 'center' }}
        style={{ width: '100%' }}
        data={state}
        renderItem={({ item }) => <CardProduct navigation={navigation} product={item} />}
        keyExtractor={product => product._id}
        numColumns={2}
      />
    </Container>
  );
};

const styles = StyleSheet.create({
  header: {
    fontSize: 35,
    textAlign: 'center',
    margin: 25,
    color: 'white',
    fontWeight: 'bold',
  },
});
export default CategoryProduct;
