import React from 'react';
import { View, StyleSheet, TextInput, Text, TouchableOpacity } from 'react-native';
import { Formik } from 'formik';
import { useAuth } from '../../hooks/useAuth';

const EditPassword = ({ navigation }) => {
  const { editMyPassword } = useAuth();

  return (
    <View style={styles.container}>
      <Formik
        initialValues={{ old_password: '', new_password: '' }}
        onSubmit={values => {
          editMyPassword({ old_password: values.old_password, new_password: values.new_password }, navigation);
        }}
      >
        {({ handleChange, handleBlur, handleSubmit, values }) => (
          <View>
            <View style={{ justifyContent: 'space-between' }}>
              <View style={[styles.inputContainer]}>
                <Text style={styles.text}>Ancien mot de passe</Text>
                <TextInput
                  onChangeText={handleChange('old_password')}
                  onBlur={handleBlur('old_password')}
                  value={values.old_password}
                  style={styles.textInput}
                  textContentType='password'
                  secureTextEntry
                />
              </View>
              <View style={styles.inputContainer}>
                <Text style={styles.text}>Nouveau mot de passe</Text>
                <TextInput
                  onChangeText={handleChange('new_password')}
                  onBlur={handleBlur('new_password')}
                  value={values.new_password}
                  style={styles.textInput}
                  textContentType='password'
                  secureTextEntry
                />
              </View>
            </View>
            <TouchableOpacity onPress={handleSubmit} style={styles.button}>
              <Text style={styles.buttonText}>Modifier mon mot de passe</Text>
            </TouchableOpacity>
          </View>
        )}
      </Formik>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    padding: 25,
    backgroundColor: '#ffffff',
  },
  inputContainer: { marginTop: 10, marginBottom: 10 },

  text: {
    marginBottom: 10,
    fontSize: 20,
  },
  textInput: {
    borderColor: 'silver',
    borderWidth: 1,
    padding: 10,
    borderRadius: 5,
  },
  button: {
    marginTop: 25,
    height: 50,
    backgroundColor: '#b33939',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: 20,
    color: 'white',
  },
});
export default EditPassword;
