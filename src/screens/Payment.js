import React, { useState, useEffect } from 'react';
import { View, ScrollView, StyleSheet, Text, TouchableOpacity, Modal, TouchableHighlight } from 'react-native';
import { useCookies } from 'react-cookie';
import SyncStorage from 'sync-storage';
import { socket as io } from '../utils/SocketIO';
import { getBasket, confirmPayment } from '../utils/API';
import { HandleError } from '../utils/HandleMessage';
import SummaryOrder from '../components/SummaryOrder';
import WaitingItem from '../components/WaitingItem';

/**
 * Page qui affiche l'étape du paiement pour l'envoyer au serveur (et au Backoffice)
 */
const Payment = ({ navigation, route }) => {
  const [socket, setSocket] = useState(null);
  const [articles, setArticles] = useState([]);
  const [reservations, setReservations] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);
  const [points, setPoints] = useState(0);
  const [modalVisible, setModalVisible] = useState(false);
  const [message, setMessage] = useState('');
  const [dataLoaded, setDataLoaded] = useState(false);
  const [, , removeCookie] = useCookies(['shoppingBasket']);

  useEffect(() => {
    setSocket(io);
    getBasket(route.params.basket)
      .then(({ status, data }) => {
        if (status === 200) {
          setReservations(data.basket.reservations);
          setArticles(data.basket.articles);
          setTotalPrice(data.basket.total_price);
          setPoints(data.basket.preview_fidelity_points);
          setDataLoaded(true);
        }
      })
      .catch(e => HandleError('Error lors de la récupération de la commande', e.response.data.error, e.response.status));
  }, [route.params.basket, SyncStorage.get('confirmBasket') === 2]);

  if (!dataLoaded) return <WaitingItem message='Récupération de la commande...' logoName='shipping-fast' />;
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Text style={{ fontSize: 20, fontWeight: 'bold', textAlign: 'center' }}>Récapitulatif de la commande</Text>

      <SummaryOrder articles={articles} reservations={reservations} price={totalPrice} points={points} />

      <View style={{ flex: 1, justifyContent: 'flex-end' }}>
        <TouchableOpacity style={styles.buttonPayment} onPress={() => confirm()}>
          <Text style={{ color: 'white', fontSize: 20 }}>Valider le paiement</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.buttonContinuePurchases} onPress={() => continuePurchases()}>
          <Text style={{ fontSize: 17 }}>Continuer mes achats</Text>
        </TouchableOpacity>
      </View>

      <Modal
        animationType='slide'
        presentationStyle='pageSheet'
        hardwareAccelerated
        statusBarTranslucent
        transparent={false}
        visible={modalVisible}
        onRequestClose={() => closeModal()}
      >
        <View style={styles.modalContainer}>
          <View>
            <Text style={{ fontSize: 20 }}>{message}</Text>

            <TouchableHighlight style={styles.buttonHideModal} onPress={() => closeModal()}>
              <Text style={{ color: 'white', fontSize: 17 }}>Revenir à l&apos;écran d&apos;accueil</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
    </ScrollView>
  );

  // ##############################
  //           FONCTIONS
  // ##############################

  function confirm() {
    confirmPayment({
      _id: route.params.basket,
      currency: 'euro',
      payment_method: 'bank card',
    })
      .then(({ status, data }) => {
        if (status === 201) {
          // s'il n'y a que des réservations, pas besoin d'envoyer au back office par websocket
          if (articles.length !== 0) socket.emit('new-order', { _id: data._id });
          // suppression des infos relatives au panier
          removeCookie('shoppingBasket');
          removeCookie('reservation');
          SyncStorage.remove('basket');
          SyncStorage.set('confirmBasket', 0);
          // mise à jour du message à afficher dans une modal
          setMessage(data.message);
          setModalVisible(true);
        }
      })
      .catch(e => HandleError('Error lors de la validation du paiement', e.response.data.error, e.response.status));
  }

  function continuePurchases() {
    SyncStorage.set('confirmBasket', 1);
    navigation.reset({
      index: 0,
      routes: [{ name: 'Accueil' }],
    });
  }

  function closeModal() {
    setModalVisible(!modalVisible);
    navigation.reset({
      index: 0,
      routes: [{ name: 'Accueil' }],
    });
  }
};
const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: 'white',
    padding: 15,
  },
  buttonPayment: {
    marginTop: 25,
    width: '100%',
    padding: 10,
    borderRadius: 25,
    backgroundColor: '#4cd137',
    alignItems: 'center',
    alignSelf: 'center',
  },
  buttonContinuePurchases: {
    marginTop: '5%',
    width: '100%',
    padding: 10,
    borderRadius: 25,
    backgroundColor: 'white',
    borderColor: 'silver',
    borderWidth: 1,
    alignItems: 'center',
    alignSelf: 'center',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonHideModal: {
    marginTop: 25,
    padding: 15,
    borderRadius: 5,
    backgroundColor: '#0652DD',
    alignItems: 'center',
    alignSelf: 'center',
  },
});
export default Payment;
