import React, { useState } from 'react';
import { View, StyleSheet, TextInput, Text, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import { Container } from '../components/Styles/Theme';
import { forgotPassword } from '../utils/API';
import { HandleError } from '../utils/HandleMessage';

const ForgotPassword = ({ navigation }) => {
  const [email, setEmail] = useState('');
  const [isValid, setIsValid] = useState(false);
  const [isReceived, setIsReceived] = useState(false);

  return (
    <Container alignItems='center' justifyContent='center' padding='10%'>
      <View style={{ width: '100%' }}>
        <TextInput
          onChangeText={value => handleChange(value)}
          style={styles.textInput}
          textContentType='emailAddress'
          value={email}
          placeholder='Votre courriel'
        />

        {requestReceived()}
      </View>
    </Container>
  );

  // Afficher un icon quand la requête a bien été reçue par le serveur
  function requestReceived() {
    return isReceived ? (
      <>
        <Icon name='check' color='green' size={75} containerStyle={{ marginTop: 25 }} />
        <Text style={{ fontSize: 18, fontWeight: 'bold', color: 'white', textAlign: 'center' }}>{`Un courriel vous a été envoyé à ${email}`}</Text>
      </>
    ) : (
      <TouchableOpacity disabled={!isValid} onPress={handleSubmit} style={[styles.button, { opacity: isValid ? 1 : 0.5 }]}>
        <Text style={styles.buttonText}>Confirmer</Text>
      </TouchableOpacity>
    );
  }

  function handleSubmit() {
    forgotPassword(email)
      .then(({ status }) => {
        if (status === 200) {
          setIsReceived(true);
          setTimeout(() => {
            navigation.goBack();
          }, 5000);
        }
      })
      .catch(e => {
        HandleError('Erreur ', e.response.data.error, e.response.status);
      });
  }

  function handleChange(value) {
    // vérification de la syntaxe du mail
    checkEmail(value);
    setEmail(value);
  }
  /**
   * Vérification de la syntaxe du courriel
   * @param {String} e email
   */
  function checkEmail(e) {
    /\S+@\S+\.\S+/.test(e) ? setIsValid(true) : setIsValid(false);
  }
};

const styles = StyleSheet.create({
  textInput: {
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 25,
    textAlign: 'center',
  },
  button: {
    marginTop: 25,
    height: 50,
    backgroundColor: '#3498db',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: 17,
    color: 'white',
  },
});
export default ForgotPassword;
