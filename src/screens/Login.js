import React from 'react';
import { View, StyleSheet, TextInput, Text, Image, TouchableOpacity } from 'react-native';
import { Formik } from 'formik';
import SyncStorage from 'sync-storage';
import { useAuth } from '../hooks/useAuth';
import { Container } from '../components/Styles/Theme';

const Login = ({ navigation }) => {
  const { signin } = useAuth();
  return (
    <Container justifyContent='center' alignItems='center'>
      <Image style={styles.image} source={require(`../../images/logo.png`)} />
      <Formik
        initialValues={{ email: SyncStorage.get('email') ? SyncStorage.get('email') : '', password: '' }}
        onSubmit={values => {
          signin(values.email, values.password);
        }}
      >
        {({ handleChange, handleBlur, handleSubmit, values }) => (
          <View style={{ width: '75%' }}>
            <View style={{ marginTop: 10, marginBottom: 10 }}>
              <TextInput
                onChangeText={handleChange('email')}
                onBlur={handleBlur('email')}
                textContentType='emailAddress'
                value={values.email}
                style={styles.textInput}
                placeholder='Courriel'
                autoCorrect
              />
            </View>
            <View style={{ marginTop: 10, marginBottom: 10 }}>
              <TextInput
                onChangeText={handleChange('password')}
                onBlur={handleBlur('password')}
                placeholder='Mot de passe'
                textContentType='password'
                value={values.password}
                secureTextEntry
                style={styles.textInput}
              />
            </View>
            <Text onPress={() => navigation.navigate('Mot de passe oublié')} style={styles.forgotPassword}>
              Mot de passe oublié ?
            </Text>
            <TouchableOpacity onPress={handleSubmit} style={styles.buttonSignin}>
              <Text style={styles.buttonText}>Se connecter</Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={() => navigation.navigate('Inscription')} style={styles.buttonSignup}>
              <Text style={styles.buttonText}>S&apos;inscrire</Text>
            </TouchableOpacity>
          </View>
        )}
      </Formik>
    </Container>
  );
};

const styles = StyleSheet.create({
  image: {
    width: 125,
    height: 125,
    marginBottom: 20,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  text: {
    marginBottom: 10,
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
  },
  textInput: {
    backgroundColor: 'white',
    borderColor: 'white',
    borderWidth: 1,
    padding: 10,
    borderRadius: 25,
    textAlign: 'center',
  },
  buttonSignin: {
    marginTop: 25,
    height: 50,
    backgroundColor: '#4cd137',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonSignup: {
    marginTop: 15,
    height: 50,
    backgroundColor: '#3498db',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: 20,
    color: 'white',
  },
  signup: {
    fontSize: 17,
    marginTop: '5%',
    textDecorationLine: 'underline',
    textAlign: 'center',
  },
  forgotPassword: {
    fontSize: 15,
    textDecorationLine: 'underline',
    textAlign: 'right',
  },
});

export default Login;
