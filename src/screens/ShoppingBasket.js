import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, TouchableOpacity, ScrollView } from 'react-native';
import { useCookies } from 'react-cookie';
import { View } from 'react-native-animatable';
import { Container } from '../components/Styles/Theme';
import CardBasket from '../components/Cards/CardBasket';
import CardTotalPrice from '../components/Cards/CardTotalPrice';
import EmptyItem from '../components/EmptyItem';
import CardBasketReservation from '../components/Cards/CardBasketReservation';
import useBasket from '../hooks/useBasket';

/**
 * SyncStorage.get('confirmBasket')
 * 0 : état initial
 * 1 : si l'utilisateur confirme son panier mais décide de continuer ses achats
 * 2 : panier confirmé
 */

/**
 * Page qui affiche le panier de l'utilisateur
 */
const ShoppingBasket = ({ navigation }) => {
  const [cookie] = useCookies(['shoppingBasket', 'user', 'reservation']);
  const [totalPrice, setTotalPrice] = useState(0);
  const [fidelityPoints, setFidelityPoints] = useState(0);
  const { confirmShoppingBasket, deleteItemIntoBasket, deleteReservationIntoBasket, downQuantity, upQuantity } = useBasket(navigation);

  useEffect(() => {
    let price = 0.0;
    let points = 0.0;

    if (cookie.shoppingBasket) {
      cookie.shoppingBasket.forEach(item => {
        price += item.totalPrice;
        points += item.points;
      });
    }
    if (cookie.reservation) {
      cookie.reservation.forEach(item => {
        price += item.totalPrice;
        points += item.points;
      });
    }
    setTotalPrice(price);
    setFidelityPoints(points);
  }, [cookie.shoppingBasket, cookie.reservation]);

  // si le panier est vide
  if ((!cookie.shoppingBasket || cookie.shoppingBasket.length === 0) && (!cookie.reservation || cookie.reservation.length === 0))
    return <EmptyItem logoName='shopping-basket' message='Panier vide' />;

  // affichage du panier
  return (
    <Container>
      <View style={{ height: '85%', width: '100%' }}>
        <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}>
          {showReservations()}

          {showArticles()}
          <CardTotalPrice price={totalPrice} points={fidelityPoints} />
        </ScrollView>
      </View>

      {/* BOUTON POUR CONFIRMER LA COMMANDE */}
      <TouchableOpacity style={styles.button} onPress={() => confirmShoppingBasket(totalPrice, fidelityPoints)}>
        <Text style={styles.buttonText}>Valider la commande</Text>
      </TouchableOpacity>
    </Container>
  );

  // ##############################
  //           FONCTIONS
  // ##############################

  function showReservations() {
    if (cookie.reservation)
      return cookie.reservation.map((item, index) => (
        <CardBasketReservation key={index.toString()} removeItem={() => deleteReservationIntoBasket(index)} reservation={item} />
      ));
    return null;
  }

  function showArticles() {
    if (cookie.shoppingBasket)
      return cookie.shoppingBasket.map((item, index) => (
        <CardBasket
          key={index.toString()}
          downQuantity={() => downQuantity(index)}
          upQuantity={() => upQuantity(index)}
          removeItem={() => deleteItemIntoBasket(index)}
          product={item}
        />
      ));
    return null;
  }
};

const styles = StyleSheet.create({
  button: {
    alignSelf: 'center',
    position: 'absolute',
    bottom: 15,
    elevation: 100,
    zIndex: 100,
    width: '87.5%',
    height: 50,
    backgroundColor: '#4cd137',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: 20,
    color: 'white',
  },
  remove: {
    borderRadius: 5,
    fontWeight: 'bold',
    width: 20,
    padding: 2,
    alignSelf: 'center',
    textAlign: 'center',
    color: 'white',
    backgroundColor: 'red',
  },
  confirmBasket: {
    padding: 10,
    marginTop: 25,
    borderRadius: 5,
    backgroundColor: '#009432',
    alignSelf: 'center',
  },
  previewFidelityPoints: {
    marginTop: 50,
    marginLeft: 5,
  },
});
export default ShoppingBasket;
