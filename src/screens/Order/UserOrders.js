import React, { useEffect, useState } from 'react';
import { StyleSheet, ScrollView, Text, View } from 'react-native';
import { ListItem } from 'react-native-elements';
import { getUserOrders } from '../../utils/API';
import { dateConverterWithHours } from '../../utils/DateConverter';
import { HandleError } from '../../utils/HandleMessage';
import WaitingItem from '../../components/WaitingItem';
import EmptyItem from '../../components/EmptyItem';

/**
 * Page qui affiche toutes les commandes de l'utilisateur
 */
const UserOrders = ({ navigation }) => {
  const [dataLoaded, setDataLoaded] = useState(false);
  const [state, setState] = useState([]);
  useEffect(() => {
    getUserOrders()
      .then(({ status, data }) => {
        if (status === 200) {
          setState(data);
          setDataLoaded(true);
        }
      })
      .catch(e => HandleError('Error lors de la récupération des commandes', e.response.data.error, e.response.status));
  }, []);

  // récupération des commandes
  if (!dataLoaded) return <WaitingItem message='Récupération des commandes' logoName='shipping-fast' />;

  // si pas de commande
  if (dataLoaded && state.length === 0) return <EmptyItem message='Aucune commande' logoName='shipping-fast' />;

  return (
    <ScrollView contentContainerStyle={styles.container}>
      {state.length > 0 ? (
        state.map((order, index) => (
          <ListItem
            chevron={{ iconStyle: { fontSize: 25 } }}
            onPress={() => navigation.navigate('Commande', { _id: order._id })}
            key={index.toString()}
            title={order.id_order}
            subtitle={dateConverterWithHours(order.createdAt)}
            rightTitle={`${order.price}€`}
            pad={30}
            bottomDivider
          />
        ))
      ) : (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text style={styles.emptyOrder}>Aucune commande</Text>
        </View>
      )}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: { flexGrow: 1, backgroundColor: 'white' },
  emptyOrder: { fontSize: 17, margin: 10 },
  loadOrder: { fontSize: 17, margin: 10 },
});
export default UserOrders;
