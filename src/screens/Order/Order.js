import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Text, ScrollView } from 'react-native';
import { getOrder } from '../../utils/API';
import { dateConverterWithDayWithHours } from '../../utils/DateConverter';
import { HandleError } from '../../utils/HandleMessage';
import SummaryOrder from '../../components/SummaryOrder';

/**
 * Page qui affiche un ecommande spécifique d'un utilisateur
 */
const Order = ({ route }) => {
  const [state, setState] = useState({});
  const [paymentMethod, setPaymentMethod] = useState('Espèce');
  const [dataLoaded, setDataLoaded] = useState(false);
  const [articles, setArticles] = useState([]);
  const [reservations, setReservations] = useState([]);

  useEffect(() => {
    getOrder(route.params._id)
      .then(({ status, data }) => {
        if (status === 200) {
          setState(data);
          setArticles(data.shopping_basket_id.articles);
          setReservations(data.shopping_basket_id.reservations);
          setDataLoaded(true);
          if (data.payment_method === 'bank card') setPaymentMethod('Carte bancaire');
        }
      })
      .catch(e => HandleError('Error lors de la récupération de la commande', e.response.data.error, e.response.status));
  }, []);

  if (!dataLoaded) return <View />;
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View>
        <Text style={{ fontSize: 17 }}>{`Commande : ${state.id_order}`}</Text>
        <Text style={{ fontSize: 15, color: 'silver' }}>{dateConverterWithDayWithHours(state.createdAt)}</Text>
      </View>
      <View>
        <Text style={{ fontSize: 17, marginTop: 10 }}>{`Méthode de paiement : ${paymentMethod}`}</Text>
        <Text style={{ fontSize: 15, color: 'silver' }}>{state.status === 1 ? 'commande payée' : 'commande non payée'}</Text>
      </View>

      <SummaryOrder articles={articles} reservations={reservations} price={state.price} points={state.fidelity_points} />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: 'white',
    padding: 10,
  },
});
export default Order;
