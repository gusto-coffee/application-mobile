import React, { useState, useEffect } from 'react';
import { FlatList, StyleSheet } from 'react-native';
import { getCoworkingCategories } from '../utils/API';
import { HandleError } from '../utils/HandleMessage';
import { Container } from '../components/Styles/Theme';
import CardCategoryCoworking from '../components/Cards/CardCategoryCoworking';

const Coworking = ({ navigation }) => {
  const [state, setState] = useState([]);

  useEffect(() => {
    getCoworkingCategories()
      .then(({ data, status }) => {
        if (status === 200) {
          setState(data.sort((a, b) => a.price - b.price));
        }
      })
      .catch(e => HandleError('Error lors de la récupération des espaces de travail', e.response.data.error, e.response.status));
  }, []);

  return (
    <Container alignItems='center'>
      <FlatList
        style={{ width: '100%' }}
        contentContainerStyle={styles.container}
        data={state}
        numColumns={1}
        renderItem={({ item }) => <CardCategoryCoworking navigation={navigation} category={item} />}
        keyExtractor={item => item._id}
      />
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    paddingBottom: '5%',
  },
});

export default Coworking;
