import React, { useState, useEffect } from 'react';
import { View, ScrollView, StyleSheet, Text, Image } from 'react-native';
import { CalendarList, LocaleConfig } from 'react-native-calendars';
import { getCoworkingPlace } from '../utils/API';
import { HandleError } from '../utils/HandleMessage';
import { Container } from '../components/Styles/Theme';
import ShowImage from '../utils/ShowImage';
import WaitingItem from '../components/WaitingItem';

LocaleConfig.locales.fr = {
  monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
  monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
  dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
  dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
  today: "Aujourd'hui",
};
LocaleConfig.defaultLocale = 'fr';

const Place = ({ route, navigation }) => {
  const [state, setState] = useState({});
  const [dataLoaded, setDataLoaded] = useState(false);

  useEffect(() => {
    getCoworkingPlace(route.params._id)
      .then(({ data, status }) => {
        if (status === 200) {
          setState(data);
          setDataLoaded(true);
        }
      })
      .catch(e => HandleError("Error lors de la récupération de l'emplacement", e.response.data.error, e.response.status));
  }, [route.params._id]);

  if (!dataLoaded) return <WaitingItem message="Récupération de l'espace de travail..." logoName='laptop-house' />;
  return (
    <Container>
      <Image style={[styles.image, { opacity: 0.5, backgroundColor: 'black' }]} source={ShowImage(state.location_category_id.name)} />
      <View style={styles.descriptionContainer}>
        <Text style={styles.descriptionText}>{`“ ${state.location_category_id.stuff} ”`}</Text>
      </View>
      <Text style={styles.header}>{`${state.name} - ${state.location_category_id.price}€ / heure`}</Text>
      <Text style={styles.subheader}>{`+ ${state.location_category_id.points} points`}</Text>

      <ScrollView style={styles.scrollContainer} contentContainerStyle={{ flexGrow: 1, padding: '5%' }}>
        <View>
          <CalendarList
            // Enable horizontal scrolling, default = false
            horizontal
            // Enable paging on horizontal, default = false
            pagingEnabled
            // Set custom calendarWidth.
            calendarWidth={320}
            onDayPress={({ day, month, year }) => {
              navigation.navigate('Reservation', { state, day, month, year, _id: route.params._id });
            }}
          />
        </View>
      </ScrollView>
    </Container>
  );
};

const styles = StyleSheet.create({
  scrollContainer: {
    flex: 1,
    backgroundColor: 'white',
    width: '100%',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  header: {
    fontSize: 30,
    marginTop: 15,
    fontWeight: 'bold',
    color: 'white',
    alignSelf: 'center',
  },
  subheader: {
    fontSize: 20,
    marginTop: 5,
    marginBottom: 15,
    fontWeight: 'bold',
    color: 'white',
    alignSelf: 'center',
  },
  descriptionContainer: {
    alignSelf: 'center',
    justifyContent: 'center',
    height: '33.33%',
    position: 'absolute',
    zIndex: 100,
    elevation: 100,
  },
  descriptionText: {
    fontSize: 20,
    fontStyle: 'italic',
    textAlign: 'center',
    color: 'white',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: { width: -2, height: 2 },
    textShadowRadius: 10,
  },
  image: {
    width: '100%',
    height: '33.33%',
  },
  text: { margin: 5 },
});

export default Place;
