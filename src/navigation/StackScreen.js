import React from 'react';
import { Image, TouchableOpacity, StyleSheet } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import Home from '../screens/Home';
import Profile from '../screens/Profile/Profile';
import ShoppingBasket from '../screens/ShoppingBasket';
import Coworking from '../screens/Coworking';
import CategoryProduct from '../screens/Category/CategoryProduct';
import Product from '../screens/Product';
import Place from '../screens/Place';
import CategoryCoworking from '../screens/Category/CategoryCoworking';
import Reservation from '../screens/Reservation';
import Promotion from '../screens/Promotion';
import EditPassword from '../screens/Profile/EditPassword';
import EditProfile from '../screens/Profile/EditProfile';
import Payment from '../screens/Payment';
import UserOrders from '../screens/Order/UserOrders';
import Order from '../screens/Order/Order';
import Login from '../screens/Login';
import Signup from '../screens/Signup';
import ForgotPassword from '../screens/ForgotPassword';

// logo en haut au milieu de l'écran
function LogoTitle({ navigation }) {
  return (
    <TouchableOpacity onPress={() => navigation.navigate('Accueil', { screen: 'Home' })}>
      <Image style={styles.logo} source={require(`../../images/logo.png`)} />
    </TouchableOpacity>
  );
}

// Pages de connexion avec sa déclinaison inscription si l'utilisateur n'a pas de compte
const UserStack = createStackNavigator();

export function UserStackScreen() {
  return (
    <UserStack.Navigator>
      <UserStack.Screen name='Connexion' options={{ title: 'Connexion' }} component={Login} />
      <UserStack.Screen name='Inscription' options={{ title: 'Inscription' }} component={Signup} />
      <UserStack.Screen name='Mot de passe oublié' options={{ title: 'Mot de passe oublié' }} component={ForgotPassword} />
    </UserStack.Navigator>
  );
}

// Page d'accueil (catalogue des produits) avec ses déclinaisons
const HomeStack = createStackNavigator();

export function HomeStackScreen() {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name='Accueil'
        options={({ navigation }) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={Home}
      />
      <HomeStack.Screen
        options={({ navigation }) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        name="Catégorie d'un produit"
        component={CategoryProduct}
      />
      <HomeStack.Screen
        options={({ navigation }) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        name='Produit'
        component={Product}
      />
    </HomeStack.Navigator>
  );
}

// Page espaces de travail avec ses déclinaisons
const CoworkingStack = createStackNavigator();

export function CoworkingStackScreen() {
  return (
    <CoworkingStack.Navigator>
      <CoworkingStack.Screen
        options={({ navigation }) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        name='Espace de travail'
        component={Coworking}
      />
      <CoworkingStack.Screen
        options={({ navigation }) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        name="Catégorie d'un espace de travail"
        component={CategoryCoworking}
      />
      <CoworkingStack.Screen
        options={({ navigation }) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        name='Emplacement'
        component={Place}
      />
      <CoworkingStack.Screen
        options={({ navigation }) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        name='Reservation'
        component={Reservation}
      />
    </CoworkingStack.Navigator>
  );
}

// Page profil avec ses déclinaisons
const ProfileStack = createStackNavigator();

export function ProfileStackScreen() {
  return (
    <ProfileStack.Navigator>
      <ProfileStack.Screen
        name='Profile'
        options={({ navigation }) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={Profile}
      />
      <ProfileStack.Screen
        name='Promotion'
        options={({ navigation }) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={Promotion}
      />
      <ProfileStack.Screen
        name='Modifier mon profil'
        options={({ navigation }) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={EditProfile}
      />
      <ProfileStack.Screen
        name='Modifier mon mot de passe'
        options={({ navigation }) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={EditPassword}
      />
      <ProfileStack.Screen
        name='Commandes'
        options={({ navigation }) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={UserOrders}
      />
      <ProfileStack.Screen
        name='Commande'
        options={({ navigation }) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={Order}
      />
    </ProfileStack.Navigator>
  );
}

// Page panier avec ses déclinaisons
const BasketStack = createStackNavigator();

export function BasketStackScreen() {
  return (
    <BasketStack.Navigator>
      <BasketStack.Screen
        name='Panier'
        options={({ navigation }) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={ShoppingBasket}
      />
      <BasketStack.Screen
        name='Paiement'
        options={({ navigation }) => ({
          headerTitleAlign: 'center',
          headerTitle: () => <LogoTitle navigation={navigation} />,
        })}
        component={Payment}
      />
    </BasketStack.Navigator>
  );
}

const styles = StyleSheet.create({
  logo: {
    width: 45,
    height: 45,
  },
});
