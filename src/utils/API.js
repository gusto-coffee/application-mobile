import axios from 'axios';
import { url_api as __URL } from '../../info.json';

axios.defaults.baseURL = __URL;

/**
 * GET
 */

export function getProfile() {
  return axios.get('/auth/profile');
}

// récupérer tous les produits
export function getProducts() {
  return axios.get('/product');
}

// récupérer tous les produits
export function getPromotions() {
  return axios.get('/product/promotion');
}

// récupérer toutes les catégories
export function getCategories() {
  return axios.get('/category');
}

export function getProduct(id) {
  return axios.get(`/product/${id}`);
}

// récupérer tous les produits d'une catégorie
export function getProductsCategory(category_id) {
  return axios.get(`/product/category/${category_id}`);
}

export function getBasket(id) {
  return axios.get(`/shopping-basket/${id}`);
}

export function getUserBasket() {
  return axios.get(`/shopping-basket/user-basket`);
}

export function getOrder(id) {
  return axios.get(`/order/${id}`);
}

export function getUserOrders() {
  return axios.get(`/order`);
}

export function getCoworkingCategories() {
  return axios.get(`/coworking`);
}

export function getCoworkingPlaces() {
  return axios.get(`/coworking/location`);
}

export function getCoworkingCategoryPlaces(id) {
  return axios.get(`/coworking/${id}/location`);
}

export function getCoworkingPlace(id) {
  return axios.get(`/coworking/location/${id}`);
}

export function getDayReservation(id, day, month, year) {
  return axios.get(`/reservation/${id}`, { params: { day, month, year } });
}

/**
 * POST
 */

export function confirmBasket(data) {
  return axios.post(`/shopping-basket/confirm`, data);
}

export function confirmPayment(data) {
  return axios.post(`/order/payment`, data);
}

export function newHoursReservation(data) {
  return axios.post(`/reservation/new`, data);
}

export function forgotPassword(email) {
  return axios.post(`/auth/forgot-password`, { email });
}
/**
 * PATCH
 */
export function editProfile(data) {
  return axios.patch(`/user`, data);
}

export function editPassword(data) {
  return axios.patch(`/user/edit-password`, data);
}

// si l'on a confirmé son panier mais que l'on désire y ajouter d'autres produits
export function editConfirmBasket(data, id) {
  return axios.patch(`/shopping-basket/confirm/${id}`, data);
}

/**
 * DELETE
 */

export function removeAccount() {
  return axios.delete(`/user`);
}
