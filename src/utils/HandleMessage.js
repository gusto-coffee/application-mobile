import { Alert } from 'react-native';
import Toast from 'react-native-simple-toast';

export function HandleError(header, body, statusCode) {
  Alert.alert(header, body || `Code erreur ${statusCode}`, [{ text: 'Fermer', style: 'cancel' }]);
}

export function NotifyMessage(msg) {
  Toast.show(msg, Toast.SHORT);
}
