const ShowImage = name => {
  switch (name) {
    case 'Bières':
      return require('../../images/products/biere.jpg');
    case 'Boissons':
      return require('../../images/products/boissons.png');
    case 'Cafés':
      return require('../../images/products/cafe.jpg');
    case 'Chaudes':
      return require('../../images/products/boisson_chaude.jpg');
    case 'Chocolats':
      return require('../../images/products/chocolat_chaud.jpg');
    case 'Eau':
      return require('../../images/products/eau.jpg');
    case 'Formules':
      return require('../../images/products/formule.jpg');
    case 'Froides':
      return require('../../images/products/boisson_froide.jpg');
    case 'Jus de fruits':
      return require('../../images/products/jus_de_fruits.jpg');
    case 'Nourriture':
      return require('../../images/products/nourriture.jpg');
    case 'Salé':
      return require('../../images/products/sel.jpg');
    case 'Sandwichs':
      return require('../../images/products/sandwich.jpg');
    case 'Services':
      return require('../../images/products/service.jpg');
    case 'Sodas':
      return require('../../images/products/sucre.jpg');
    case 'Sucré':
      return require('../../images/products/sucre.jpg');
    case 'Thés':
      return require('../../images/products/the.jpg');
    case 'Viennoiseries':
      return require('../../images/products/viennoiserie_.jpg');

    // COWORKING
    case 'Petit salon privé':
      return require('../../images/coworking/petit_salon_privee.jpg');
    case 'Grand salon privé':
      return require('../../images/coworking/grand_salon_privee.jpg');
    case 'Salle principale':
      return require('../../images/coworking/principale.jpg');
    default:
      return require('../../images/160x100.png');
  }
};

export default ShowImage;
