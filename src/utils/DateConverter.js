import Moment from 'moment';
import 'moment/min/locales';

Moment.locale('fr');

/**
 *
 * @param {DateTime} datetime
 * @returns {String} value format : 27 août 2020 15h00
 */
export function dateConverterWithHours(date) {
  return Moment(date).format('lll');
}

/**
 *
 * @param {DateTime} datetime
 * @returns {String} 27 août 2020
 */
export function dateConverterWithoutHours(date) {
  return Moment(date).format('ll');
}

/**
 *
 * @param {DateTime} datetime
 * @returns {String} jeudi 27 août 2020 15h00
 */
export function dateConverterWithDayWithHours(date) {
  return Moment(date).format('dddd lll');
}

/**
 *
 * @param {DateTime} datetime
 * @returns {String} jeudi 27 août 2020
 */
export function dateConverterWithDayWithoutHours(date) {
  return Moment(date).format('dddd ll');
}

export function getHourNow() {
  return Moment().hour();
}
