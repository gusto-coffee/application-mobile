import io from 'socket.io-client';
import SyncStorage from 'sync-storage';
import { url_api as __URL } from '../../info.json';

// options de la connexion
const __OPTIONS = {
  query: { token: SyncStorage.get('token'), room: 'NWS2020' },
  secure: true,
};

export const socket = io(__URL, __OPTIONS);
