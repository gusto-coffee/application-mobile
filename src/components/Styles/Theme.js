import styled from 'styled-components';

// contenaire principal
export const Container = styled.View`
  flex: 1;
  padding-bottom: ${props => props.paddingBottom || 0};
  background-color: ${props => props.bg || '#c3a684'};
  align-items: ${props => props.alignItems || 'flex-start'};
  justify-content: ${props => props.justifyContent || 'flex-start'};
`;

// contenaire d'une page vide (pas de produit...)
export const Empty = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  background-color: ${props => props.bg || '#c3a684'};
`;

// contenaire d'une page de chargement
export const Waiting = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  background-color: ${props => props.bg || '#c3a684'};
`;
