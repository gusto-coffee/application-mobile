import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { Card } from 'react-native-elements';

/**
 * Composant qui affiche le total des achats sous forme de carte
 */
const CardTotalPrice = ({ price, points }) => (
  <View style={{ width: '100%' }}>
    <Card containerStyle={styles.cardContainer}>
      <View style={{ flexDirection: 'row' }}>
        <Text style={styles.text}>Points de fidélité :</Text>
        <Text style={styles.textRight}>{`${points}`}</Text>
      </View>
      <View style={styles.divider} />
      <View style={{ flexDirection: 'row' }}>
        <Text style={styles.text}>Total :</Text>
        <Text style={styles.textRight}>{`${price}€`}</Text>
      </View>
    </Card>
  </View>
);

const styles = StyleSheet.create({
  cardContainer: {
    borderWidth: 0,
    borderRadius: 20,
    margin: 25,
  },
  divider: {
    flexDirection: 'row',
    height: 0.5,
    backgroundColor: 'silver',
    marginTop: '2.5%',
    marginBottom: '2.5%',
  },
  text: {
    fontSize: 17,
    fontWeight: 'bold',
    color: '#7f8c8d',
  },
  textRight: {
    flex: 1,
    textAlign: 'right',
    fontSize: 17,
    fontWeight: 'bold',
    color: '#7f8c8d',
  },
});

export default CardTotalPrice;
