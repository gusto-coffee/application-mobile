import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Card } from 'react-native-elements';
import ShowImage from '../../utils/ShowImage';

/**
 * Composant qui affiche les catégories sous forme de carte
 */
const CardCategory = ({ navigation, category }) => (
  <TouchableOpacity
    onPress={() => {
      navigation.navigate("Catégorie d'un produit", { category: { _id: category._id, name: category.name } });
    }}
  >
    <Card
      title={category.name}
      wrapperStyle={{ flex: 1, justifyContent: 'space-evenly' }}
      image={ShowImage(category.name)}
      titleStyle={styles.title}
      containerStyle={styles.cardContainer}
    />
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  cardContainer: {
    borderRadius: 5,
    width: 160,
    height: 260,
    borderWidth: 0,
  },
  title: {
    fontSize: 19,
  },
});

export default CardCategory;
