import React from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';
import { Card, Icon } from 'react-native-elements';
import ShowImage from '../../utils/ShowImage';

/**
 * Composant pour afficher les réservations du panier sous forme de carte
 */
const CardBasketReservation = ({ reservation, removeItem }) => (
  <View style={{ width: '100%' }}>
    <Card wrapperStyle={{ flexDirection: 'row' }} containerStyle={styles.cardContainer}>
      {/* HEURES */}
      <View style={styles.quantityContainer}>
        <Text style={styles.quantityText}>{`${reservation.hours}h`}</Text>
      </View>

      <Image style={{ width: 100, height: 100, borderRadius: 10 }} source={ShowImage(reservation.category)} />

      {/* BLOC TEXTE A DROITE DE L'IMAGE */}
      <View style={{ marginLeft: 15, flex: 1 }}>
        {/* NOM DE L'ESPACE */}
        <Text style={{ fontSize: 17, fontWeight: 'bold' }}>{reservation.name}</Text>

        <View style={{ flex: 1 }}>
          <View style={{ flex: 1, alignItems: 'center', margin: 25 }}>
            <Text style={styles.hoursText}>{new Date(reservation.date.year, reservation.date.month - 1, reservation.date.day).toDateString()}</Text>
            <Text style={[styles.hoursText, { marginLeft: 10 }]}>{`${reservation.reservedHours.start}h - ${reservation.reservedHours.end}h`}</Text>
          </View>

          <View style={{ marginTop: '5%', marginBottom: '5%' }}>
            <Text style={[styles.priceText, { fontSize: 15, textAlign: 'right' }]}>{`+ ${reservation.points} points`}</Text>
          </View>

          {/* PRIX TOTAL */}
          <View style={styles.priceContainer}>
            <Text style={styles.priceText}>{`${reservation.totalPrice}€`}</Text>
          </View>
        </View>
      </View>
    </Card>

    {/* BOUTON SUPPRIMER (CROIX) */}
    <Icon containerStyle={styles.iconContainer} reverse size={17} color='#EA2027' type='font-awesome-5' name='times' onPress={removeItem} />
  </View>
);

const styles = StyleSheet.create({
  cardContainer: {
    borderWidth: 0,
    borderRadius: 20,
    margin: 25,
  },
  title: {
    fontSize: 19,
  },
  priceContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  optionContainer: {
    fontSize: 17,
    color: 'silver',
    fontWeight: 'bold',
    marginLeft: 10,
  },
  iconContainer: {
    position: 'absolute',
    elevation: 100,
    zIndex: 100,
    top: 5,
    right: 5,
  },
  quantityContainer: {
    borderRadius: 100,
    borderRightWidth: 2,
    borderBottomWidth: 2,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderColor: 'white',
    backgroundColor: '#b33939',
    padding: 5,
    position: 'absolute',
    elevation: 100,
    zIndex: 100,
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    top: -25,
    left: -25,
  },
  quantityText: {
    textAlign: 'center',
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
  },
  priceText: {
    fontSize: 17,
    color: '#7f8c8d',
    fontWeight: 'bold',
  },
  hoursText: {
    fontSize: 17,
    color: 'silver',
    fontWeight: 'bold',
  },
});

export default CardBasketReservation;
