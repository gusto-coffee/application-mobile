import React from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { Card, Icon } from 'react-native-elements';
import ShowImage from '../../utils/ShowImage';

/**
 * Composant pour afficher les produits du panier sous forme de carte
 */
const CardBasket = ({ product, removeItem, upQuantity, downQuantity }) => {
  return (
    <View style={{ width: '100%' }}>
      <Card
        wrapperStyle={{ flexDirection: 'row' }}
        containerStyle={[styles.cardContainer, { borderWidth: product.promo ? 5 : 0, borderColor: '#fff200' }]}
      >
        {/* QUANTITE */}
        <View style={styles.quantityContainer}>
          <Text style={styles.quantityText}>{`${product.quantity}`}</Text>
        </View>

        <Image style={{ width: 100, height: 100, borderRadius: 10 }} source={ShowImage(product.categories[0].name)} />

        {/* BLOC TEXTE A DROITE DE L'IMAGE */}
        <View style={{ marginLeft: 15, flex: 1 }}>
          {/* NOM DU PRODUIT */}
          <Text style={{ fontSize: 17, fontWeight: 'bold' }}>{product.name}</Text>

          {/* OPTIONS */}
          <View style={{ flexGrow: 1 }}>
            {product.options.map((opt, index) => (
              <Text key={index.toString()} style={styles.optionContainer}>{`+ ${opt.name}`}</Text>
            ))}
            {showPoints()}
          </View>

          <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-end' }}>
            {showQuantityButtons()}

            {/* PRIX TOTAL DE L'ARTICLE */}
            <View style={styles.priceContainer}>
              <Text style={styles.priceText}>{`${product.totalPrice}€`}</Text>
            </View>
          </View>
        </View>
      </Card>

      {/* BOUTON SUPPRIMER (CROIX) */}
      <Icon containerStyle={styles.iconContainer} reverse size={17} color='#EA2027' type='font-awesome-5' name='times' onPress={removeItem} />
    </View>
  );

  /**
   * Affichage des boutons pour augmenter/diminuer la quantité si ce n'est pas une promotion
   * sinon on affiche les points consommés
   */
  function showQuantityButtons() {
    return !product.promo ? (
      <>
        <TouchableOpacity onPress={() => downQuantity()} style={styles.quantityButtonLeft}>
          <Text style={styles.quantityText}>-</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => upQuantity()} style={[styles.quantityButtonRight, { marginLeft: 2 }]}>
          <Text style={styles.quantityText}>+</Text>
        </TouchableOpacity>
      </>
    ) : (
      <View>
        <Text style={styles.priceText}>PROMO</Text>
        <Text style={[styles.priceText, { fontSize: 15 }]}>{`${product.points} points`}</Text>
      </View>
    );
  }

  /**
   * Afficher le nombre de points que l'utilisateur bénéficie avec le produit
   */
  function showPoints() {
    return !product.promo ? (
      <View style={{ marginTop: '5%' }}>
        <Text style={[styles.priceText, { fontSize: 15, textAlign: 'right' }]}>{`+ ${product.points} points`}</Text>
      </View>
    ) : null;
  }
};

const styles = StyleSheet.create({
  cardContainer: {
    borderRadius: 20,
    margin: 25,
  },
  title: {
    fontSize: 19,
  },
  priceContainer: {
    flex: 1,
    marginTop: '15%',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  optionContainer: {
    fontSize: 17,
    color: 'silver',
    fontWeight: 'bold',
    marginLeft: 10,
  },
  iconContainer: {
    position: 'absolute',
    elevation: 100,
    zIndex: 100,
    top: 5,
    right: 5,
  },
  quantityContainer: {
    borderRadius: 100,
    borderRightWidth: 2,
    borderBottomWidth: 2,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderColor: 'white',
    backgroundColor: '#b33939',
    padding: 5,
    position: 'absolute',
    elevation: 100,
    zIndex: 100,
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    top: -25,
    left: -25,
  },
  quantityText: {
    textAlign: 'center',
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
  },
  priceText: {
    fontSize: 17,
    color: '#7f8c8d',
    fontWeight: 'bold',
  },
  quantityButtonLeft: {
    padding: 4,
    borderTopLeftRadius: 25,
    borderBottomLeftRadius: 25,
    backgroundColor: '#b33939',
    width: '20%',
  },
  quantityButtonRight: {
    padding: 4,
    borderTopRightRadius: 25,
    borderBottomRightRadius: 25,
    backgroundColor: '#b33939',
    width: '20%',
  },
});

export default CardBasket;
