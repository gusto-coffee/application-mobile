import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Card } from 'react-native-elements';
import ShowImage from '../../utils/ShowImage';

/**
 * Composant qui affiche les catégories des espaces de travail sous forme de carte
 */
const CardCategoryCoworking = ({ navigation, category }) => (
  <TouchableOpacity
    style={{ margin: 15, borderRadius: 5 }}
    onPress={() => {
      navigation.navigate("Catégorie d'un espace de travail", {
        category: { _id: category._id, name: category.name, price: category.price, place_number: category.place_number },
      });
    }}
  >
    <Card
      title={`${category.name}\n\n${category.place_number} ${category.place_number > 1 ? 'places' : 'place'}`}
      featuredSubtitle={`${category.price}€ / h`}
      featuredSubtitleStyle={styles.featuredSubtitle}
      image={ShowImage(category.name)}
      titleStyle={styles.title}
      containerStyle={styles.cardContainer}
    />
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  cardContainer: {
    margin: 0,
    borderWidth: 0,
    borderRadius: 5,
    width: 300,
  },
  title: {
    fontSize: 19,
  },
  featuredSubtitle: {
    fontSize: 40,
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: { width: -2, height: 2 },
    textShadowRadius: 10,
  },
});

export default CardCategoryCoworking;
