import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Card } from 'react-native-elements';
import ShowImage from '../../utils/ShowImage';

/**
 * Composant qui affiche le produit sous forme de carte
 */
const CardProduct = ({ navigation, product }) => (
  <TouchableOpacity
    style={{ borderRadius: 5 }}
    onPress={() => {
      navigation.navigate('Produit', { _id: product._id });
    }}
  >
    <Card
      title={product.name}
      image={ShowImage(product.category_id[0].name)}
      containerStyle={styles.cardContainer}
      featuredSubtitle={`${product.price}€`}
      featuredSubtitleStyle={styles.featuredSubtitle}
    />
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  cardContainer: {
    borderRadius: 5,
    borderWidth: 0,
    width: 160,
  },
  price: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  featuredSubtitle: {
    fontSize: 40,
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: { width: -2, height: 2 },
    textShadowRadius: 10,
  },
});

export default CardProduct;
