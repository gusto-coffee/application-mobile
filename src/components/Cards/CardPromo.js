import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Badge, Avatar } from 'react-native-elements';
import { useAuth } from '../../hooks/useAuth';
import ShowImage from '../../utils/ShowImage';

/**
 * Composant qui affiche la promo du produit sous forme de carte
 * et si l'utilisateur peut ou non en bénéficier en fonction de ses points
 */
const CardPromo = ({ product, addProductIntoShoppingBasket }) => {
  const { user } = useAuth();

  return (
    <View style={styles.container}>
      <View
        style={{
          borderRadius: 5,
          borderColor: 'silver',
          borderWidth: 0.5,
        }}
      >
        <Avatar avatarStyle={{ borderRadius: 5 }} containerStyle={{ padding: 10 }} source={ShowImage(product.category_id[0].name)} size='xlarge' />
        <Badge
          status='warning'
          value={product.offer}
          textStyle={{ fontSize: 20 }}
          badgeStyle={styles.badge}
          containerStyle={{ position: 'absolute', top: -10, right: -15 }}
        />
        {promoAvailable()}
      </View>
      <Text style={styles.text}>{product.name}</Text>
    </View>
  );

  function promoAvailable() {
    return product.offer <= user.fidelity_points ? (
      <TouchableOpacity style={styles.button} onPress={addProductIntoShoppingBasket}>
        <Text style={{ textAlign: 'center', color: 'white', fontSize: 17, fontWeight: 'bold' }}>Convertir</Text>
      </TouchableOpacity>
    ) : (
      <Text style={{ padding: 7.5, textAlign: 'center', fontSize: 17, color: '#7f8c8d', margin: 10 }}>
        {`${product.offer - user.fidelity_points} pts restants`}
      </Text>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    margin: 15,
    padding: 5,
  },
  button: {
    flex: 1,
    margin: 10,
    padding: 7.5,
    backgroundColor: '#c3a684',
    borderRadius: 5,
  },
  text: {
    flex: 1,
    marginTop: 10,
    textAlign: 'center',
    textAlignVertical: 'center',
    fontSize: 15,
    width: '90%',
    height: '20%',
    alignSelf: 'center',
  },
  badge: {
    height: 50,
    width: 50,
    borderWidth: 2,
    borderColor: 'white',
    backgroundColor: '#b33939',
    borderRadius: 100,
  },
});

export default CardPromo;
