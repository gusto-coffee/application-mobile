import React from 'react';
import { Text } from 'react-native';
import { Icon } from 'react-native-elements';
import { Empty } from './Styles/Theme';

// Composant qui affiche le rendu d'une page vide avec un logo
const EmptyItem = ({ message, logoName = 'info-circle' }) => (
  <Empty>
    <Text style={{ fontSize: 30, color: 'white', fontWeight: 'bold', margin: 10, textAlign: 'center' }}>{message}</Text>
    <Icon name={logoName} type='font-awesome-5' color='white' size={40} />
  </Empty>
);

export default EmptyItem;
