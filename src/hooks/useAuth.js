import axios from 'axios';
import React, { createContext, useContext, useState } from 'react';
import { useCookies } from 'react-cookie';
import SyncStorage from 'sync-storage';
import { editPassword, editProfile, removeAccount } from '../utils/API';
import { HandleError, NotifyMessage } from '../utils/HandleMessage';

const authContext = createContext();
// Provider qui crée l'objet d'auth et gère les states
const useProvideAuth = () => {
  const [cookie, setCookie, removeCookie] = useCookies(['user']);
  const [user, setUser] = useState(cookie.user);
  const today = new Date();
  const tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);

  const signin = (email, password) => {
    axios
      .post('/auth/login', {
        email,
        password,
      })
      .then(({ data, status }) => {
        if (status === 200) {
          setUser(data.user);
          setCookie('user', JSON.stringify(data.user), {
            expires: tomorrow,
          });
          // ajout du token dans le header des requêtes http
          axios.defaults.headers.authorization = `Baerer ${data.user.token}`;
          // ajout de l'email en stockage local pour le garder en mémoire lors d'une déconnexion
          SyncStorage.set('email', data.user.email);
          SyncStorage.set('token', data.user.token);
        }
      })
      .catch(e => {
        HandleError('Error lors de la connexion', e.response.data.error, e.response.status);
      });
  };

  const signup = (lastname, firstname, email, password) => {
    axios
      .post('/auth/signup', {
        lastname,
        firstname,
        email,
        password,
      })
      .then(({ data, status }) => {
        if (status === 201) {
          setUser(data.user);
          setCookie('user', JSON.stringify(data.user), {
            expires: tomorrow,
          });
          // ajout du token dans le header des requêtes http
          axios.defaults.headers.authorization = `Baerer ${data.user.token}`;
          // ajout de l'email en stockage local pour le garder en mémoire lors d'une déconnexion
          SyncStorage.set('email', data.user.email);
          SyncStorage.set('token', data.user.token);
        }
      })
      .catch(e => {
        HandleError("Error lors de l'inscription", e.response.data.error[0].msg);
      });
  };

  const signout = () => {
    removeCookie('user');
    setUser(undefined);
    SyncStorage.remove('token');
  };

  const editMyProfile = (userData, navigation) => {
    editProfile(userData)
      .then(({ status, data }) => {
        if (status === 201) {
          setUser(data.user);
          setCookie('user', JSON.stringify(data.user), {
            expires: tomorrow,
          });
          // ajout du token dans le header des requêtes http
          axios.defaults.headers.authorization = `Baerer ${data.user.token}`;
          // ajout de l'email en stockage local pour le garder en mémoire lors d'une déconnexion
          SyncStorage.set('email', data.user.email);
          SyncStorage.set('token', data.user.token);
          NotifyMessage('Profil modifié avec succès !');
          navigation.goBack();
        }
      })
      .catch(e => {
        HandleError('Error lors de la modification du profil', e.response.data.error, e.response.status);
      });
  };

  const editMyPassword = (passwords, navigation) => {
    editPassword(passwords)
      .then(({ status }) => {
        if (status === 201) {
          NotifyMessage('Mot de passe modifié avec succès !');
          navigation.goBack();
        }
      })
      .catch(e => {
        HandleError('Error lors de la modification du mot de passe', e.response.data.error, e.response.status);
      });
  };

  const removeMyAccount = () => {
    removeAccount()
      .then(({ status }) => {
        if (status === 200) {
          // suppression du stockage local
          const keys = SyncStorage.getAllKeys();
          keys.forEach(key => SyncStorage.remove(key));
          // suppression du cookie
          removeCookie('user');
          // mise à jour des valeurs
          setUser(undefined);
          axios.defaults.headers.authorization = undefined;
        }
      })
      .catch(e => {
        HandleError('Error lors de la suppression du profil', e.response.data.error, e.response.status);
      });
  };

  // Subscribe to user on mount

  // Because this sets state in the callback it will cause any ...

  // ... component that utilizes this hook to re-render with the ...

  // ... latest auth object.

  //   useEffect(() => {
  //     const unsubscribe = firebase.auth().onAuthStateChanged(user => {
  //       if (user) {
  //         setUser(user);
  //       } else {
  //         setUser(false);
  //       }
  //     });

  //     // Cleanup subscription on unmount

  //     return () => unsubscribe();
  //   }, []);

  return {
    user,
    setUser,
    signin,
    signup,
    signout,
    editMyProfile,
    editMyPassword,
    removeMyAccount,
  };
};

// Provider qui englobe l'app et rend l'objet d'auth
// accessible a tous les composants qui apppellent useAuth()
export function AuthProvider({ children }) {
  const auth = useProvideAuth();
  return <authContext.Provider value={auth}>{children}</authContext.Provider>;
}

// Hook pour les composants enfants
// afin de disposer de l'objet d'auth et re-render quand il change
export const useAuth = () => {
  return useContext(authContext);
};
