import { useState, useEffect } from 'react';
import { useCookies } from 'react-cookie';
import SyncStorage from 'sync-storage';
import { isEqual } from 'lodash';
import { socket as io } from '../utils/SocketIO';
import { confirmBasket, editConfirmBasket } from '../utils/API';
import { HandleError, NotifyMessage } from '../utils/HandleMessage';

const today = new Date();
const tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);

const useBasket = navigation => {
  const [cookie, setCookie] = useCookies(['shoppingBasket', 'user', 'reservation']);
  const [socket, setSocket] = useState(null);

  useEffect(() => {
    setSocket(io);
  }, []);

  // ajouter un produit dans le panier
  function addProductIntoBasket(product, quantity, checkbox) {
    const basket = cookie.shoppingBasket || [];
    const options_checked = checkbox.filter(cb => cb.isChecked);
    let priceOptions = 0.0;
    // ajout du prix des options
    options_checked.forEach(option => {
      priceOptions += option.price;
    });
    // prix unitaire de l'article
    const productPrice = product.price;

    // ajout du prix total de l'article ((produit unitaire + options)*quantité)
    const totalPrice = (productPrice + priceOptions) * quantity;

    // logique pour augmenter la quantité si jamais on rajoute le même produit avec les mêmes options
    // afin d'éviter de recréer un article
    if (basket.find(e => e._id === product._id && isEqual(e.options, options_checked)))
      basket.forEach((item, index) => {
        if (item._id === product._id && isEqual(item.options, options_checked)) {
          basket[index].quantity += quantity;
          basket[index].points += product.points;
          basket[index].totalPriceOptions += priceOptions * quantity;
          basket[index].totalPrice += totalPrice;
        }
      });
    else
      basket.push({
        _id: product._id,
        name: product.name,
        categories: product.category_id,
        productPrice,
        totalPrice,
        quantity,
        productPoints: product.points,
        points: product.points * quantity,
        options: options_checked,
        totalPriceOptions: priceOptions * quantity,
        priceOptions,
        promo: false,
      });

    setCookie('shoppingBasket', basket, {
      expires: tomorrow,
    });
    NotifyMessage(`${product.name} ajouté`);
  }

  // confirmer le panier pour passer à l'étape du paiement
  function confirmShoppingBasket(totalPrice, fidelityPoints) {
    const articles = [];
    const reservations = [];

    if (cookie.shoppingBasket) {
      cookie.shoppingBasket.forEach(item => {
        const option_ids = [];
        // ajout des options
        item.options.forEach(option => {
          option_ids.push(option._id);
        });
        // mise en forme de la requête des articles
        articles.push({
          product_id: item._id,
          option_ids,
          quantity: item.quantity,
          price: item.totalPrice,
        });
      });
    }
    if (cookie.reservation) {
      cookie.reservation.forEach(item => {
        reservations.push({
          reservation_id: item._id,
          price: item.totalPrice,
        });
      });
    }
    // envoie de la confirmation de la commande au serveur
    if (SyncStorage.get('confirmBasket') === 1)
      editConfirmBasket(
        {
          articles,
          reservations,
          total_price: totalPrice,
          preview_fidelity_points: fidelityPoints,
        },
        SyncStorage.get('basket'),
      )
        .then(({ status, data }) => {
          if (status === 201) {
            // sauvegarde de l'identifiant du panier
            // ajout d'une valeur de confirmation pour pouvoir revenir à ses achats
            SyncStorage.set('basket', data._id);
            SyncStorage.set('confirmBasket', 2);
            navigation.navigate('Paiement', { basket: data._id });
          }
        })
        .catch(e => HandleError('Error lors de la confirmation du panier', e.response.data.error, e.response.status));
    else
      confirmBasket({
        articles,
        reservations,
        total_price: totalPrice,
        preview_fidelity_points: fidelityPoints,
      })
        .then(({ status, data }) => {
          if (status === 201) {
            // sauvegarde de l'identifiant du panier
            // ajout d'une valeur de confirmation pour pouvoir revenir à ses achats
            SyncStorage.set('basket', data._id);
            SyncStorage.set('confirmBasket', 2);
            navigation.navigate('Paiement', { basket: data._id });
          }
        })
        .catch(e => HandleError('Error lors de la confirmation du panier', e.response.data.error, e.response.status));
  }

  // supprimer un article du panier
  function deleteItemIntoBasket(index) {
    if (cookie.shoppingBasket) {
      // stockage du panier dans une variable
      const newShoppingBasket = cookie.shoppingBasket;

      // si c'est un produit ajouté par promotion
      // on remet le compteur des points de fidélité à jour
      if (newShoppingBasket[index].promo) {
        // mise à jour du cookie avec les nouveaux points de fidélité
        const updateUserCookie = cookie.user;
        updateUserCookie.fidelity_points -= newShoppingBasket[index].points;
        setCookie('user', updateUserCookie, {
          expires: tomorrow,
        });
        //
      }
      // suppression de l'article selectionné
      newShoppingBasket.splice(index, 1);

      // mise à jour du cookie avec les nouvelles valeurs
      setCookie('shoppingBasket', newShoppingBasket);
    }
  }

  // supprimer une réservation du panier
  function deleteReservationIntoBasket(index) {
    if (cookie.reservation) {
      socket.emit('remove-hour-reservation', { _id: cookie.reservation[index]._id }, response => {
        if (response.code === 200) {
          // stockage du panier dans une variable
          const newReservation = cookie.reservation;
          // suppression de l'article selectionné
          newReservation.splice(index, 1);

          // mise à jour du cookie avec les nouvelles valeurs
          setCookie('reservation', newReservation);
        }
      });
    }
  }

  // bouton pour baisser la quantité d'un article du panier
  function downQuantity(index) {
    if (cookie.shoppingBasket) {
      // stockage du panier dans une variable
      const newShoppingBasket = cookie.shoppingBasket;

      // mise à jour du cookie avec les nouveaux points de fidélité
      const updateUserCookie = cookie.user;
      updateUserCookie.fidelity_points -= newShoppingBasket[index].productPoints;
      setCookie('user', updateUserCookie, {
        expires: tomorrow,
      });
      // mise à jour de la quantité
      newShoppingBasket[index].quantity--;
      // mise à jour du prix des options
      newShoppingBasket[index].totalPriceOptions -= newShoppingBasket[index].priceOptions;
      // mise à jour du prix
      newShoppingBasket[index].totalPrice -= newShoppingBasket[index].productPrice + newShoppingBasket[index].priceOptions;
      // mise à jour des points de fidélité
      newShoppingBasket[index].points -= newShoppingBasket[index].productPoints;

      // suppression de l'article si la quantité est nulle
      if (newShoppingBasket[index].quantity === 0) newShoppingBasket.splice(index, 1);

      // mise à jour du cookie avec les nouvelles valeurs
      setCookie('shoppingBasket', newShoppingBasket);
    }
  }

  // bouton pour augmenter la quantité d'un article du panier
  function upQuantity(index) {
    if (cookie.shoppingBasket) {
      // stockage du panier dans une variable
      const newShoppingBasket = cookie.shoppingBasket;

      // mise à jour du cookie avec les nouveaux points de fidélité
      const updateUserCookie = cookie.user;
      updateUserCookie.fidelity_points += newShoppingBasket[index].productPoints;
      setCookie('user', updateUserCookie, {
        expires: tomorrow,
      });
      // mise à jour de la quantité
      newShoppingBasket[index].quantity++;
      // mise à jour du prix des options
      newShoppingBasket[index].totalPriceOptions += newShoppingBasket[index].priceOptions;
      // mise à jour du prix
      newShoppingBasket[index].totalPrice += newShoppingBasket[index].productPrice + newShoppingBasket[index].priceOptions;
      // mise à jour des points de fidélité
      newShoppingBasket[index].points += newShoppingBasket[index].productPoints;

      // mise à jour du cookie avec les nouvelles valeurs
      setCookie('shoppingBasket', newShoppingBasket);
    }
  }

  return {
    addProductIntoBasket,
    confirmShoppingBasket,
    deleteItemIntoBasket,
    deleteReservationIntoBasket,
    downQuantity,
    upQuantity,
  };
};

export default useBasket;
